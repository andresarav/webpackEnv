import React from 'react'
import { render } from 'react-dom'
import HelloWorld from './components/hello'

const renderized = document.getElementById('app')
render(<HelloWorld/>, renderized)
